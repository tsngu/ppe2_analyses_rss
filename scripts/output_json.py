#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  7 09:33:01 2023

@author: agathew
"""

from datastructures import Article, Corpus, Token


import extraire_un as e1

def article_to_json(article: Article):
    art = {}
    art['title'] = article.titre
    art['description'] = article.description
    art['analyse']=[]
    for token in article.analyse:
        token_dict={"forme":token.forme, "lemme": token.lemme, "pos":token.pos, "dep":token.dep, "head":token.head}
        art['analyse'].append(token_dict)
    #print(art)
    return art

def output_json(corpus: Corpus):
    dico_resultat = {}
    for article in corpus.articles:
        if article.date not in dico_resultat:
            dico_resultat[article.date] = {}
        if article.categorie not in dico_resultat[article.date]:
            dico_resultat[article.date][article.categorie] = []
        dico_resultat[article.date][article.categorie].append(article_to_json(article))
    return dico_resultat
