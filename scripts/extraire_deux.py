#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author : tsngu, agathew

S'exécute avec
$ python3 ./scripts/extraire_deux.py ./corpus/2022/ cat1 cat2 cat3 -s 2022-mm-dd -e 2022-mm-dd -o path/output/nomfichier.extensionsouhaitée

"""

import argparse, sys, re, pickle, json

from extraire_un import extract_re
from datastructures import Corpus
from output_xml import output_xml
from output_json import output_json
from output_pickle import output_pickle



from datetime import date, datetime
from pathlib import Path
from tagging import tag_article
from get_infos import conv_mois
from tqdm import tqdm

dico_cat={"une":"0,2-3208,1-0,0", 
          "international":"0,2-3210,1-0,0", 
          "europe":"0,2-3214,1-0,0",
          "societe":"0,2-3224,1-0,0", 
          "idees":"0,2-3232,1-0,0",
          "economie":"0,2-3234,1-0,0",
          "actualite-medias":"0,2-3236,1-0,0",
          "sport":"0,2-3242,1-0,0",
          "planete":"0,2-3244,1-0,0",
          "culture":"0,2-3246,1-0,0",
          "livres":"0,2-3260,1-0,0",    
          "cinema":"0,2-3476,1-0,0",
          "voyage":"0,2-3546,1-0,0",
          "technologies":"0,2-651865,1-0,0",
          "politique":"0,57-0,64-823353,0",
          "sciences":"env_sciences"}

mois=["Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"]
           
jours=[f"{j:02}" for j in range(1,32)]


def parcours_path(root_path, categories, start_date, end_date):
    if len(categories)>0 :  
        cat_list=[dico_cat[cat] for cat in categories]
    else :
        cat_list=dico_cat.values()
    for month_dir in root_path.iterdir():
        if month_dir.name not in mois:
            continue
        m = conv_mois(mois, month_dir.name)
        for day_dir in month_dir.iterdir():
            if day_dir.name not in jours:
                continue
            d = date.fromisoformat(f"2022-{m:02}-{day_dir.name}")
            if start_date<=d and d<=end_date:
                for time_dir in day_dir.iterdir():
                    if re.match(r"\d\d-\d\d-\d\d", time_dir.name):
                        for file in time_dir.iterdir():
                            if file.name.endswith(".xml") and any([c in file.name for c in cat_list]):
                                yield file


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", nargs="*", help="start date (iso format)", default="2022-01-01")
    parser.add_argument("-e", nargs="*", help="end date (iso format)", default="2023-01-01")
    parser.add_argument("-o", nargs="*", help="output file (stdout if not specified)")
    parser.add_argument("path", help="root dir of the corpus data")
    parser.add_argument("categories",nargs="*", help="catégories à retenir")
    args = parser.parse_args()

    rp = Path(args.path)
    if "-s" in sys.argv and "-e" in sys.argv:
        start_date=date.fromisoformat(args.s[0])
        end_date=date.fromisoformat(args.e[0])
    elif "-s" in sys.argv and "-e" not in sys.argv:
        start_date=date.fromisoformat(args.s[0])
        end_date=date.fromisoformat(args.e)
    elif "-s" not in sys.argv and "-e" in sys.argv:
        start_date=date.fromisoformat(args.s)
        end_date=date.fromisoformat(args.e[0])
    else:
        start_date=date.fromisoformat(args.s)
        end_date=date.fromisoformat(args.e)
    categories=args.categories
    output_path=[]
    if "-o" in sys.argv:
        output_path=args.o

   
    corpus=Corpus(categories, start_date, end_date, rp, [], [], 0.0)
    total_files = len(list(parcours_path(rp, categories, start_date, end_date)))
    with tqdm(total=total_files, unit='file', ncols=80, bar_format='{l_bar}{bar}| {n_fmt}/{total_fmt}') as pbar:
        for file in parcours_path(rp, categories, start_date, end_date):
            for article in extract_re(file, dico_cat):
                corpus.articles.append(article)
                article.analyse=tag_article(article)
            pbar.update(1)

    corpus.articles.sort(key=lambda x: datetime.strptime(x.date, '%d %b %Y'))

    if output_path:
        for p in output_path:
            if ".xml" in p:
                with open (p, "w", encoding="utf-8") as wf:
                    wf.write(output_xml(corpus))
            if ".json" in p:
                with open(p, "w", encoding="utf-8") as wf:
                    json.dump(output_json(corpus), wf, ensure_ascii=False, indent=2)
            if ".pickle" in p or ".pkl" in p:
                with open(p, "wb") as wf:
                    pickle.dump(output_pickle(corpus), wf)
    else:
        for article in corpus.articles:
            print(f"date : {article.date}\ncat : {article.categorie}\ntitle : {article.titre}\ndesc : {article.description}\n\n")