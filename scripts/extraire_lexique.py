#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  13 10:33:34 2023

@author: agathew, FlorianJacquot, tsngu
"""

import glob, argparse, sys
import pprint as pp


def liste_contenu_fichiers():
	parseur = argparse.ArgumentParser(prog="ReadFiles", description='Read files from the corpus')
	parseur.add_argument("path", nargs="*", help="permet de donner les fichiers d'entrée en argument")
	parseur.add_argument("-c", "--cat", nargs="?", help="textes avec un fichier du corpus par ligne")
	parseur.add_argument("-l", "--ls", nargs="?", help="liste des chemins des fichiers à lire")
	args = parseur.parse_args()

	if args.path :
		path=args.path
		liste_textes=[]
		for file in path:
			with open(file,"r") as f:
				contenu=f.read()
				liste_textes.append(contenu)
		return liste_textes
	
	if args.ls is None or args.cat is None:
		if sys.argv[1] == "-c" or sys.argv[1] == "--cat":
			cat = ""
			for line in sys.stdin:
				cat += line
			liste_textes_cat = cat.split("\n")
			return liste_textes_cat
		elif sys.argv[1] == "-l" or sys.argv[1] == "--ls":
			liste_textes_ls=[]
			paths_list = [line.strip() for line in sys.stdin]
			for path in paths_list:
				with open(path, 'r') as f:
					content=f.read()
					liste_textes_ls.append(content)
			return liste_textes_ls


# Prend une liste de chaîne et compte les occurences de chaque mots.
def nb_occurrences(liste_chaine):
	mots = {}
	for string in liste_chaine:
		for word in string.split():
			if word in mots:
				mots[word] += 1
			else:
				mots[word] = 1
	return mots


# fonction pour associer les mots au nombre de documents dans lequel il apparaît
def termInDocFreq(liste_chaine):
	dico = {}
	for chaine in liste_chaine:
		l_mots = chaine.strip().split()
		for mot in l_mots:
			if mot not in dico:
				dico[mot] = 0
	for chaine in liste_chaine:
		for mot in dico:
			if mot in chaine:
				dico[mot] += 1
	# pp.pprint(dico)
	return dico
 	

def lex_corpus():
	TF_IDF=[]
	liste_textes=liste_contenu_fichiers()
	dico_TF=nb_occurrences(liste_textes)
	dico_IDF=termInDocFreq(liste_textes)
	TF_IDF.append(dico_TF)
	TF_IDF.append(dico_IDF)
	return TF_IDF


#liste_textes=liste_contenu_fichiers()
#print(len(liste_textes))
#print(liste_textes)

TF_IDF = lex_corpus()
TF=TF_IDF[0]
IDF=TF_IDF[1]
pp.pprint(TF)
pp.pprint(IDF)

