#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Mon Apr 10 2023

author : @tsngu
"""

from datastructures import Corpus, Article, Token
import spacy as sp

nlp = sp.load("fr_core_news_sm")

def tag_article(article: Article):
    content=article.titre + " " + article.description
    doc = nlp(content)
    tokens = []
    for token in doc:
        if not token.is_stop and not token.is_punct:
            tokens.append(Token(
                forme=token.text,
                lemme=token.lemma_,
                pos=token.pos_,
                dep=token.dep_,
                head=token.head.text if token.head else ""
            ))

    return tokens

